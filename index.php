<?php require_once('init.php'); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Fusion Client Core SDK - Presence Sample</title>
        <link rel="stylesheet" href="css/smoothness/jquery-ui-1.10.2.custom.min.css">
        <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.10.2.custom.min.js"></script>
        <style>
            .ui-tabs-vertical { width: 55em; }
            .ui-tabs-vertical .ui-tabs-nav { padding: .2em .1em .2em .2em; float: left; width: 12em; }
            .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
            .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
            .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; border-right-width: 1px; }
            .ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: right; width: 40em;}
            span { font-size: 0.6em; }
        </style>
    </head>
    <body>
        <h2>You are: <?php print $_GET['ps_username']; ?></h2>
        <form class='details'>
            <p>
                I am: 
                <select id='status'>
                    <option value='OFFLINE'>Offline</option>
                    <option value='AVAILABLE'>Available</option>
                    <option value='AWAY'>Away</option>
                    <option value='BUSY'>Busy</option>
                </select>
                and I'm feeling:
                <input type='text' id='status-message'>
                <input type='submit' value='Update'>
            </p>
        </form>
        <div id='tabs' class='ui-tabs-vertical ui-helper-clearfix'>
            <ul>
                <!-- this list will by dynamically populated with the links to the contact specific IM tabs -->
                <!-- E.g.
                <li>
                    <a href='#tab-nath'>Nath <br>
                        <span class='status'>AVAILABLE</span> 
                        <span class='status-message'>Happy</span> 
                    </a>
                </li>
                <li>
                    <a href='#tab-dave'>Dave
                        <span class='status'>AWAY</span> 
                        <span class='status-message'>Sad</span>
                    </a>
                </li>
                -->
            </ul>
            <!-- section will be dynamically populated with divs containing IMs  -->
            <!-- E.g.
            <div id="tab-nath">
                <p>Nath message 1</p>
                <p>Nath message 2</p>
            </div>
            <div id="tab-dave">
                <p>Dave message 1</p>
            </div>
            -->
        </div>
    </body>
</html>

<!-- libraries -->
<script type='text/javascript' src='http://192.168.250.15:8080/gateway/adapter.js'></script>
<script type='text/javascript' src='http://192.168.250.15:8080/gateway/fusion-client-sdk.js'></script>

<!-- setup Fusion Client SDK --> 
<script type='text/javascript'>
    
    // set up the UI
    $('#tabs').tabs();
    $('#tabs li').removeClass('ui-corner-top').addClass('ui-corner-left');
    
    // prevent the annoying UI box
    $('#tabs').on('click', '.ui-tabs-anchor', function() {
        $(this).blur();
    });

    // make the UI inactive until the UC is initialised
    $('input, select').attr('disabled', 'disabled');

    // function to add a new IM tab - as we discover each of our subscribed
    // contacts, we'll want to create a separate tab for them
    function addContactLinkAndTab (contact) {
        var tabs = $('#tabs').tabs();
        var ul = tabs.find('ul');
        
        // dynamically create the HTML for the tab link to the contact
        var $link = $(
            '<li>' + 
                '<a href="#tab-' + contact.name + '">' + 
                    contact.name + '<br />' + 
                    '<span class="status">' + contact.status + '</span> ' +
                    '<span class="status-message">' + contact.customStatusMessage).hide();
        $link.appendTo(ul);
        $link.show('slow');

        // dynamically create the HTML for the IM messaging tab for the contact
        var $newTab = $(
            '<div id="tab-' + contact.name + '">' + 
                '<h3 class="status"></h3>' +
                '<form class="im">' +
                    '<input type="text">' +
                    '<input type="submit" value="Send">');
        $newTab.appendTo(tabs);
        tabs.tabs('refresh');


        // now add the submit handler for the IM form associated
        // with this specific contact.  Doing this here will mean 
        // that the function will have closure over the contact 
        // object.  This is useful since the contact is needed 
        // to create the conversation object
        var conversation = null;
        $($newTab).find('form.im').submit(function (e) {
            e.preventDefault();
            if (!conversation) {
                // TODO - create a conversation (hint: this function is passed
                // a Contact object; use the contact's address property) & add
                // a handler for dealing with the messages that may be received
                // later (hint: take a look at the 'addMessage' function defined 
                // somewhere).
                
            }

            // TODO - create a new Message using the value of the IM field
            // note that we'll also want to setup handlers to handle
            // message delivery & delivery failure 
            // hint: when the message is delivered (onMessageDelivered), 
            // update the value of the local message log ('addMessage'?).
            var text = $(this).find('input[type="text"]').val();

            // TODO - with the handlers all set up, send the message on
            // its way
        });
    }

    // returns true if the UI has been updated with IM
    // and state tabs for the given contact
    function ensureContactAddedToUI (contact) {
        if ($('#tabs a[href="#tab-' + contact.name + '"]').length === 0)
            addContactLinkAndTab(contact);
    }

    // add a message to the tab for a given user
    function addMessage(contact, message, received) {
        var $message = $('<p>').hide();
        if (received)
            $message.text(contact.name + ': ' + message.text);
        else
            $message.text('You: ' + message.text);
        
        // ensure that the UI has been prepped for the contact
        ensureContactAddedToUI(contact);
        switchToContactsTab(contact);

        // add the message to the end of the list
        $('#tab-' + contact.name).append($message);
        $message.toggle('slow');
    }

    function switchToContactsTab (contact) {
        var $needle = $('a[href="#tab-' + contact.name + '"]');
        var index = $('#tabs li>a').index($needle);
        $('#tabs').tabs('option', 'active', index);
    }

    // when the user updates their details
    $('form.details').submit(function (e) {
        e.preventDefault();
        // TODO - retrieve the 'logged-in' users' details
        // and use the UC.presence object to update their
        // status on the server
        // hint: to get the value of their status, or their 
        // status message, you can use: $('????').val(); 
        // Replace ???? with an appropriate selector
    });

    // Client SDK init code
    window.sessionID = "<?php echo $_SESSION['sessionid']; ?>";

    UC.start(sessionID, []);

    UC.onInitialised = function () {
        $('input').removeAttr('disabled');
    };

    // once init'd setup the presence handlers
    UC.presence.onOwnStatusChange = function (status, statusMessage) {
        // in case the select is disabled, enable it
        $('select').removeAttr('disabled');
        
        // TODO - update the <select id='#status'> with the value of the
        // status you've discovered through this function
        // hint: if using jQuery, use the .val('some text') method

        // TODO - update the <input type='text' id='status-message'> with 
        // the value of the statusMessage you've discovered through this function
        // status message is optional - so check before updating UI

        // TODO - if you're currently 'OFFLINE', ask the user if they want
        // to appear online
        // hint: use JavaScript's 'confirm' function, in concert with the
        // 'setStatus' method of the Presence object.
    };

    // update the UI if our contacts state changes
    UC.presence.onContactStatusChange = function (contact) {
        // get the li containing the contact's state & message - and update them
        ensureContactAddedToUI(contact);

        $elemStatus = $('li>a[href="#tab-' + contact.name + '"] span.status');
        $elemStatusMessage = $('li>a[href="#tab-' + contact.name + '"] span.status-message');
        // TODO - the 2 jQuery objects just above already defined for you 
        // represent the parts of the UI in the DOM that should hold the
        // contact's (the status for whom we've been notified about in this 
        // method) status, and status message - these happen to both be <span>s 
        // in the <a> link (on the left of the UI).
        // 
        // You will need to update the content of these <span's>
        // hint: use the jQuery .text('some text'), or .html('some html') methods
        //
        // Note - it's typical in jQuery to use a $ at the start of a variable 
        // name to identify if the object is a jQuery.
    };

    // TODO - assign the Presence object's 'onConversationStarted' callback
    // function.  This needs to do just one thing - define the Conversation's
    // handler for dealing with messages received on that conversation.
    // hint: need to update the UI when a message is received?  Take a look at
    // the 'addMessage' function defined in this file.
    // 
    // Note: this is necessary for Conversations that another Contact 
    // started with you - there is another TODO for setting up the handler
    // for Conversation's that you instigate with some other Contact

    
</script>
